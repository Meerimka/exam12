const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  dbUrl: 'mongodb://localhost/exam12',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook:{
    appId:'335306487147326',
    appSecret: 'd9ad28ba74b98df30e59e87d70f82359'
  }
};
