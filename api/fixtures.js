const mongoose = require('mongoose');
const nanoid =require('nanoid');
const Photo =require('./models/Photo');
const User =require('./models/User');
const config = require('./config');



const run =async () => {
  await  mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for(let collection of collections){
      await collection.drop();
  }

  const user = await User.create({
    username: 'Nancy',
      password: '123456',
      displayName:'User',
      avatar: 'User.jpg',
      token: nanoid()
  });

const photo = await Photo.create(
    {
        title: "Eminem",
        image: "eminem.jpg",
        user: user._id
    },
    {
        title: "Logic",
        image: 'logic.jpg',
        user: user._id
    },
    {
        title: "LinkinPark",
        image: 'linkin.jpg',
        user: user._id
    },
);
  return connection.close();

};


run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});

