const express = require('express');
const Photo = require('../models/Photo');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middlewere/auth');
const tryAuth = require('../middlewere/tryAuth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();


router.get('/',tryAuth,async (req,res)=>{
    try {

        let criteria = {};

        if(req.query.user){
            criteria = {
                user: req.query.user
            }
        }

        const photo = await Photo.find(criteria).populate('user');
        return res.send(photo);
    }catch (e) {
        console.log(e);
        return res.status(500).send(e);
    }

});

router.post('/' , auth ,upload.single('image') , async (req,res)=>{
    try {
        const photo = new Photo({
            title: req.body.title,
            user: req.user._id,
            image: req.file.filename
        });

        await photo.save();

        res.send(photo);
        
    }catch (e) {
        if(e.name === 'ValidationError'){
            return res.status(400).send(e);
        }
        return res.status(500).send(e);
    }
});


router.delete('/:id',auth, async (req,res)=>{
    const photo = await Photo.findOne({
        _id: req.params.id,
        user: req.user._id
    });

    if(!photo){
        return res.sendStatus(403);
    }
    await photo.remove();
    res.send({message: "OK"})
});


module.exports =router;
