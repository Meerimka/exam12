import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Container} from "reactstrap";
import {Switch, Route, withRouter} from "react-router-dom";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {logoutUser} from "./store/actions/usersActions";
import Photos from "./containers/Photos/Photos";
import NewPhoto from "./containers/Photos/NewPhoto";
import {NotificationContainer} from "react-notifications";
import UserPhoto from "./containers/Photos/UserPhoto";



class App extends Component {
    render() {
        return (
            <Fragment>
                <NotificationContainer/>
                <header>
                    <Toolbar user={this.props.user}  logout={this.props.logoutUser}/>
                </header>
                <Container style={{marginTop: '20px'}}>
                    <Switch >
                        <Route path="/" exact component={Photos}/>
                        <Route path="/photos/new" exact component={NewPhoto}/>
                        <Route path="/photos/:id" exact component={UserPhoto}/>
                        <Route path="/register" exact component={Register}/>
                        <Route path="/login" exact component={Login}/>
                    </Switch>
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state =>({
    user: state.users.user
});
const mapDispatchToProps = dispatch =>({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(App));
