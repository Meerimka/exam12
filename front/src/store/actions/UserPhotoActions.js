import axios from '../../axios-api';

export const FETCH_PHOTO_SUCCESS = 'FETCH_PHOTO_SUCCESS';


export const fetchPhotoSuccess = photos => ({type: FETCH_PHOTO_SUCCESS, photos});


export const fetchUserPhotos= (id) => {
    return dispatch => {
        return axios.get(`/photos/?user=${id}`).then(
            response => dispatch(fetchPhotoSuccess(response.data))
        );
    };
};
