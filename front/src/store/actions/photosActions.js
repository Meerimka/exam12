import axios from '../../axios-api';
import {push} from "connected-react-router";
import {NotificationManager} from 'react-notifications';
import {fetchUserPhotos} from "./UserPhotoActions";


export const FETCH_PHOTO_SUCCESS = 'FETCH_PHOTO_SUCCESS';
export const CREATE_PHOTO_SUCCESS = 'CREATE_PHOTO_SUCCESS';
export const FETCH_FAIL = 'FETCH_FAIL';

export const fetchPhotoSuccess = photos => ({type: FETCH_PHOTO_SUCCESS, photos});
export const createPhotoSuccess = () => ({type: CREATE_PHOTO_SUCCESS});
export const fetchFail = error => ({type: FETCH_FAIL, error});

export const fetchPhotos = () => {
    return (dispatch , getState)=> {
        const user = getState().users.user;
        if(user === null){
            return axios.get('/photos').then(
                response => {
                    dispatch(fetchPhotoSuccess(response.data))
                }
            );
        }else {
            return axios.get('/photos',{headers: {'Token': user.token}}).then(
                response => {
                    dispatch(fetchPhotoSuccess(response.data))
                }
            );
        }
    };
};

export const createPhoto= photoData => {
    return (dispatch, getState) => {

        const user = getState().users.user;
        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.post('/photos', photoData, {headers: {'Token': user.token}}).then(
                () => {

                    dispatch(createPhotoSuccess());
                    dispatch(fetchPhotos());
                   NotificationManager.success('Your photo is  added');
                }
            );
        }

    };
};


export const deletePhoto = (Id) => {
    return (dispatch,getState) => {
        const user = getState().users.user;
        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.delete('/photos/' + Id, {headers: {'Token': user.token}} ).then(
                () => {
                    dispatch(push('/'));
                    NotificationManager.success('Deleted photo  successfully');
                },
                error => dispatch(fetchFail(error))
            );
        }

    }
};



