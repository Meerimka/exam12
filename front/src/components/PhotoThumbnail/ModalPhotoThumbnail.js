import React from 'react';

import NotFound from '../../assets/images/not found.png';
import {apiURL} from "../../constants";

const style = {
    width: '100%',
    height: '100%',
    marginRight: '10px'
};

const ModalPhotoThumbnail = props => {
    let image = NotFound;

    if (props.image) {
        image = apiURL + '/uploads/' + props.image;
    }





    return <img src={image} style={style} className="img-thumbnail" alt="Something here" />
};

export default ModalPhotoThumbnail;