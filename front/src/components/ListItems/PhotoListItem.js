import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody} from "reactstrap";
import {Link} from "react-router-dom";
import '../../containers/Photos/Photo.css';
import PhotoThumbnail from "../PhotoThumbnail/PhotoThumbnail";

const PhotoListItem = props => {
    return (
        <Card className="Photo-card" style={{'marginTop': '10px', cursor: "pointer"}} onClick={()=>props.showModal(props)}>
            <CardBody>
                <PhotoThumbnail image={props.image}/>
                <h2>{props.title}</h2>
                <p>By:
                    <Link to={'/photos/' + props.user._id}>
                        {props.user.displayName}
                    </Link>
                     </p>
            </CardBody>
        </Card>
    );
};

PhotoListItem.propTypes ={
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    user: PropTypes.object,
    image: PropTypes.string,
    showModal: PropTypes.func.isRequired

};
export default PhotoListItem;