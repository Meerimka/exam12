import React from 'react';
import PropTypes from 'prop-types';
import {Button, ButtonGroup, Card, CardBody} from "reactstrap";
import '../../containers/Photos/Photo.css';
import PhotoThumbnail from "../PhotoThumbnail/PhotoThumbnail";

const UserPhotoListItem = props => {
    return (
        <Card className="Photo-card" style={{'marginTop': '10px', cursor: "pointer"}} onClick={()=>props.showModal(props)} >
            <CardBody>
                <PhotoThumbnail image={props.image}/>
                <h2><span className="text-muted">Title: &nbsp;</span>
                   {props.title}
                </h2>
                <p>By: {props.user.displayName}</p>

                {props.author &&  props.author._id === props.user._id? <ButtonGroup>
                    <Button color="danger" onClick={props.deletePhoto}>Delete</Button>
                </ButtonGroup> : null}

            </CardBody>
        </Card>
    );
};

UserPhotoListItem.propTypes ={
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string,
    user: PropTypes.object,
    deletePhoto: PropTypes.func,
    author: PropTypes.object,
    showModal: PropTypes.func.isRequired
};
export default UserPhotoListItem;