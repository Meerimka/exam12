import React, {Component, Fragment} from 'react';
import {Button, Modal, ModalBody, ModalFooter} from 'reactstrap';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import UserPhotoListItem from "../../components/ListItems/UserPhotoListItem";
import {deletePhoto} from "../../store/actions/photosActions";
import {fetchUserPhotos} from "../../store/actions/UserPhotoActions";
import './Photo.css'
import ModalPhotoThumbnail from "../../components/PhotoThumbnail/ModalPhotoThumbnail";


class UserPhoto extends Component {

    state={
        selectedItem:null
    };

    showModal = photo =>{
        this.setState({selectedItem: photo})
    };

    hideModal = () =>{
        this.setState({selectedItem: null})
    };



    componentDidMount() {
        this.props.onFetchUserPhoto(this.props.match.params.id);
    }

    componentDidUpdate(prevProps) {
        if(this.props.match.params.id !== prevProps.match.params.id){
            this.props.onFetchUserPhoto(this.props.match.params.id);
        }
    }


    render() {
        const photo = this.props.photos.map(photo => (
                <UserPhotoListItem
                    showModal ={()=>this.showModal(photo)}
                    key={photo._id}
                    _id={photo._id}
                    title={photo.title}
                    image={photo.image}
                    user = {photo.user}
                    author = {this.props.user}
                    deletePhoto={()=>this.props.deletePhoto(photo._id)}
                />
            ));

        return (
            <Fragment>
                {this.props.photos[0] ?
                    <h2>
                        {this.props.photos[0].user.displayName} Gallery
                        {this.props.user && this.props.user._id === this.props.photos[0].user._id ?
                            <Link to="/photos/new">
                                <Button
                                    color="primary"
                                    className="float-right"
                                > Add new Photo
                                </Button>
                            </Link> : null
                        }
                    </h2>
                    :<div>PLease upload photos </div>}

                <div className="Photos">
                    {photo}
                </div>

                <Modal isOpen={!!this.state.selectedItem} toggle={this.hideModal}>
                    {this.state.selectedItem && (
                        <Fragment>
                            <ModalBody>
                                <ModalPhotoThumbnail   image={this.state.selectedItem.image}/>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={this.hideModal}>Cancel</Button>
                            </ModalFooter>
                        </Fragment>
                    )}

                </Modal>


            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    photos: state.photos.photos,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onFetchUserPhoto: (id) => dispatch(fetchUserPhotos(id)),
    deletePhoto: (id)=>dispatch(deletePhoto(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPhoto);


