import React, {Component, Fragment} from 'react';
import {Button, Modal, ModalBody, ModalFooter} from 'reactstrap';
import {connect} from "react-redux";
import PhotoListItem from "../../components/ListItems/PhotoListItem";
import './Photo.css';
import {fetchPhotos} from "../../store/actions/photosActions";
import ModalPhotoThumbnail from "../../components/PhotoThumbnail/ModalPhotoThumbnail";


class Photos extends Component {
    state={
        selectedItem:null
    };

    showModal = photo =>{
        this.setState({selectedItem: photo})
    };

    hideModal = () =>{
        this.setState({selectedItem: null})
    };

    componentDidMount() {
        this.props.onFetchPhotos();
    }

    render() {
        const photo= this.props.photos.map(photo => (
            <PhotoListItem
                showModal ={()=>this.showModal(photo)}
                key={photo._id}
                _id={photo._id}
                title={photo.title}
                image={photo.image}
                user = {photo.user}
            />
        ));
        return (
            <Fragment>
                <h2>
                    Gallery


                </h2>
                <div className="Photos">
                    {photo}
                </div>

                <Modal isOpen={!!this.state.selectedItem} toggle={this.hideModal}>
                    {this.state.selectedItem && (
                        <Fragment>
                            <ModalBody>
                                <ModalPhotoThumbnail   image={this.state.selectedItem.image}/>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={this.hideModal}>Cancel</Button>
                            </ModalFooter>
                        </Fragment>
                    )}

                </Modal>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    photos: state.photos.photos,

});

const mapDispatchToProps = dispatch => ({
    onFetchPhotos: () => dispatch(fetchPhotos())
});

export default connect(mapStateToProps, mapDispatchToProps)(Photos);


