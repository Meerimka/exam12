import React, {Component} from 'react';
import {connect} from "react-redux";
import {Col, Button, Form, FormGroup, Input, Label} from "reactstrap";
import {createPhoto} from "../../store/actions/photosActions";



class NewPhoto extends Component {

    state = {
        title: '',
        image: null,
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.photoCreated(formData);
        this.props.history.push('/');

    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
                <Form onSubmit={this.submitFormHandler}>
                    <h2>New photo</h2>
                    <FormGroup row>
                        <Label sm={2} for="title">Title</Label>
                        <Col sm={10}>
                            <Input
                                type="text" required
                                name="title" id="title"
                                placeholder="Enter product title"
                                value={this.state.title}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="image">Image</Label>
                        <Col sm={10}>
                            <Input

                                type="file" required
                                name="image" id="image"
                                onChange={this.fileChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset:2, size: 10}}>
                            <Button type="submit" color="primary">Save</Button>
                        </Col>
                    </FormGroup>
                </Form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    photoCreated: photoData => dispatch(createPhoto(photoData))
});

export default connect(null, mapDispatchToProps)(NewPhoto);