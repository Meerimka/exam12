import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {registerUser} from "../../store/actions/usersActions";
import {connect} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import FacebookLogin from "../../components/FacebookLogin";

class Register extends Component {
  state = {
    username: '',
    password: '',
      displayName: '',
      avatar: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();

      const formData = new FormData();

      Object.keys(this.state).forEach(key => {
          formData.append(key, this.state[key])
      });

      this.props.registerUser(formData);
      this.props.history.push('/');

  };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

  getFieldError = fieldName => {
    return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
  };

  render() {
    return (
      <Fragment>
        <h2>Register new user</h2>
        {this.props.error && this.props.error.global && (
          <Alert color="danger">
            {this.props.error.global}
          </Alert>
        )}

        <Form onSubmit={this.submitFormHandler}>
            <FormGroup>
                <FacebookLogin/>
            </FormGroup>
         <FormElement
             propertyName="username"
             title="UserName"
             type="text"
             value={this.state.username}
             onChange={this.inputChangeHandler}
             error={this.getFieldError('username')}
             placeholder="Enter your desired username"
             autoComplete="new-username"
         />
            <FormElement
                propertyName="displayName"
                title="displayName"
                type="text"
                value={this.state.displayName}
                onChange={this.inputChangeHandler}
                placeholder="Enter your desired displayName"
            />

            <FormGroup row>
                <Label sm={2} for="avatar">Artist image</Label>
                <Col sm={10}>
                    <Input
                        type="file"
                        name="avatar" id="avatar"
                        onChange={this.fileChangeHandler}
                    />
                </Col>
            </FormGroup>
          <FormElement
              propertyName="password"
              title="Password"
              type="password"
              value={this.state.password}
              onChange={this.inputChangeHandler}
              error={this.getFieldError('password')}
              placeholder="Enter new secure password"
              autoComplete="new-password"
          />

          <FormGroup row>
            <Col sm={{offset: 2, size: 10}}>
              <Button type="submit" color="primary">
                Register
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
  registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
